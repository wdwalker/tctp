const WorkoutTypes = {
  REST: {
    name: 'Rest Day',
    shortName: 'REST',
    description: 'Off the bike.',
  },
  EASY: {
    name: 'Easy; Active Recovery; Warmup; Cooldown',
    shortName: 'EASY',
    description: 'Light gearing. Low pedal resistance.',
    rpe: 2,
    heart: [0.50, 0.68],
    power: [0.45, 0.50],
    cadence: [85, 95],
    page: -1,
  },
  FP: {
    name: 'Fast Pedal',
    shortName: '  FP',
    description: 'Light gearing. Low pedal resistance.',
    rpe: 7,
    heart: null,
    power: null,
    cadence: [108, 120],
    page: 91,
  },
  EM: {
    name: 'Endurance Miles',
    shortName: '  EM',
    description: 'Moderate-paced endurance intensity.',
    rpe: 5,
    heart: [0.50, 0.91],
    power: [0.45, 0.73],
    cadence: [85, 95],
    page: 92,
  },
  T: {
    name: 'Tempo',
    shortName: '   T',
    description: 'Sublactate for developing aerobic power and endurance.',
    rpe: 6,
    heart: [0.88, 0.90],
    power: [0.80, 0.85],
    cadence: [70, 75],
    page: 93,
  },
  SS: {
    name: 'Steady State',
    shortName: '  SS',
    description: 'Close to lactate for increasing maximum sustainable power.',
    rpe: 7,
    heart: [0.92, 0.94],
    power: [0.86, 0.90],
    cadence: [85, 95],
    page: 93,
  },
  CR: {
    name: 'Climbing Repeats',
    shortName: '  CR',
    description: 'Like Steady State, but reflect additional workload to go uphill.',
    rpe: 8,
    heart: [0.95, 0.97],
    power: [0.95, 1.00],
    cadence: [70, 85],
    page: 94,
  },
  SEPI: {
    name: 'Steady Effort Power Intervals',
    shortName: 'SEPI',
    description: 'Reach power target within 30-40sec and maintain.',
    rpe: 10,
    heart: [1.00, 1.00],
    power: [1.01, 1.30],
    cadence: [90, 100],
    page: 94,
  },
  PFPI: {
    name: 'Peak and Facde Power Intervals',
    shortName: 'PFPI',
    description: 'Reach power target within 30-40sec and maintain.',
    rpe: 10,
    heart: [1.00, 1.00],
    power: [1.01, 1.30],
    cadence: [90, 100],
    page: 95,
  },
  UI: {
    name: 'Over Under Intervals (Under)',
    shortName: '  UI',
    description: 'Paired with Over.',
    rpe: 9,
    heart: [0.92, 0.94],
    power: [0.86, 0.90],
    cadence: [85, 95],
    page: 96,
  },
  OI: {
    name: 'Over Under Intervals (Over)',
    shortName: '  OI',
    description: 'Paired with Under. Always want to end on an OI.',
    rpe: 9,
    heart: [0.95, 0.97],
    power: [0.95, 1.00],
    cadence: [85, 95],
    page: 96,
  },
};

export default WorkoutTypes;
