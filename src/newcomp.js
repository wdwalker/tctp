import types from './types';

const workouts = [
  {
    name: 'Week 1 - Mon',
    type: types.EM,
    segments: [
      { type: types.EASY, duration: 10 },
      { type: types.EM, duration: 40 },
      { type: types.EASY, duration: 10 },
    ],
  },
  {
    name: 'Week 1 - Tue',
    type: types.SS,
    segments: [
      { type: types.EASY, duration: 10 },
      { type: types.EM, duration: 30 },
      { type: types.SS, duration: 8 },
      { type: types.EASY, duration: 5 },
      { type: types.SS, duration: 8 },
      { type: types.EASY, duration: 5 },
      { type: types.SS, duration: 8 },
      { type: types.EASY, duration: 10 },
    ],
  },
  {
    name: 'Week 1 - Wed',
    type: types.REST,
  },
  {
    name: 'Week 1 - Thu',
    type: types.SS,
    segments: [
      { type: types.EASY, duration: 10 },
      { type: types.EM, duration: 30 },
      { type: types.SS, duration: 8 },
      { type: types.EASY, duration: 5 },
      { type: types.SS, duration: 8 },
      { type: types.EASY, duration: 5 },
      { type: types.SS, duration: 8 },
      { type: types.EASY, duration: 10 },
    ],
  },
  {
    name: 'Week 1 - Fri',
    type: types.REST,
  },
  {
    name: 'Week 1 - Sat',
    type: types.EM,
    segments: [
      { type: types.EASY, duration: 10 },
      { type: types.EM, duration: 80 },
      { type: types.EASY, duration: 10 },
    ],
  },
  {
    name: 'Week 1 - Sun',
    type: types.EM,
    segments: [
      { type: types.EASY, duration: 10 },
      { type: types.EM, duration: 80 },
      { type: types.EASY, duration: 10 },
    ],
  },
  {
    name: 'Week 2 - Mon',
    type: types.REST,
  },
  {
    name: 'Week 2 - Tue',
    type: types.SS,
    segments: [
      { type: types.EASY, duration: 10 },
      { type: types.EM, duration: 30 },
      { type: types.SS, duration: 10 },
      { type: types.EASY, duration: 6 },
      { type: types.SS, duration: 10 },
      { type: types.EASY, duration: 6 },
      { type: types.SS, duration: 10 },
      { type: types.EASY, duration: 10 },
    ],
  },
  {
    name: 'Week 2 - Wed',
    type: types.REST,
  },
  {
    name: 'Week 2 - Thu',
    type: types.SS,
    segments: [
      { type: types.EASY, duration: 10 },
      { type: types.EM, duration: 30 },
      { type: types.SS, duration: 10 },
      { type: types.EASY, duration: 6 },
      { type: types.SS, duration: 10 },
      { type: types.EASY, duration: 6 },
      { type: types.SS, duration: 10 },
      { type: types.EASY, duration: 10 },
    ],
  },
  {
    name: 'Week 2 - Fri',
    type: types.REST,
  },
  {
    name: 'Week 2 - Sat',
    type: types.EM,
    segments: [
      { type: types.EASY, duration: 10 },
      { type: types.EM, duration: 80 },
      { type: types.EASY, duration: 10 },
    ],
  },
  {
    name: 'Week 2 - Sun',
    type: types.EM,
    segments: [
      { type: types.EASY, duration: 10 },
      { type: types.EM, duration: 80 },
      { type: types.EASY, duration: 10 },
    ],
  },
  {
    name: 'Week 3 - Mon',
    type: types.REST,
  },
  {
    name: 'Week 3 - Tue',
    type: types.SEPI,
    segments: [
      { type: types.EASY, duration: 10 },
      { type: types.EM, duration: 30 },
      { type: types.SEPI, duration: 3 },
      { type: types.EASY, duration: 3 },
      { type: types.SEPI, duration: 3 },
      { type: types.EASY, duration: 3 },
      { type: types.SEPI, duration: 3 },
      { type: types.EASY, duration: 8 },
      { type: types.SEPI, duration: 3 },
      { type: types.EASY, duration: 3 },
      { type: types.SEPI, duration: 3 },
      { type: types.EASY, duration: 3 },
      { type: types.SEPI, duration: 3 },
      { type: types.EASY, duration: 10 },
    ],
  },
  {
    name: 'Week 3 - Wed',
    type: types.REST,
  },
  {
    name: 'Week 3 - Thu',
    type: types.UI,
    segments: [
      { type: types.EASY, duration: 10 },
      { type: types.EM, duration: 30 },
      { type: types.UI, duration: 3 },
      { type: types.OI, duration: 3 },
      { type: types.UI, duration: 3 },
      { type: types.EASY, duration: 6 },
      { type: types.UI, duration: 3 },
      { type: types.OI, duration: 3 },
      { type: types.UI, duration: 3 },
      { type: types.EASY, duration: 6 },
      { type: types.UI, duration: 3 },
      { type: types.OI, duration: 3 },
      { type: types.UI, duration: 3 },
      { type: types.EASY, duration: 10 },
    ],
  },
  {
    name: 'Week 3 - Fri',
    type: types.REST,
  },
  {
    name: 'Week 3 - Sat',
    type: types.UI,
    segments: [
      { type: types.EASY, duration: 10 },
      { type: types.EM, duration: 30 },
      { type: types.UI, duration: 3 },
      { type: types.OI, duration: 3 },
      { type: types.UI, duration: 3 },
      { type: types.EASY, duration: 5 },
      { type: types.UI, duration: 3 },
      { type: types.OI, duration: 3 },
      { type: types.UI, duration: 3 },
      { type: types.EASY, duration: 5 },
      { type: types.UI, duration: 3 },
      { type: types.OI, duration: 3 },
      { type: types.UI, duration: 3 },
      { type: types.EASY, duration: 10 },
    ],
  },
  {
    name: 'Week 3 - Sun',
    type: types.EM,
    segments: [
      { type: types.EASY, duration: 10 },
      { type: types.EM, duration: 90 },
      { type: types.EASY, duration: 10 },
    ],
  },

];

export default workouts;
