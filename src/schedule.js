/* eslint-disable no-console */
import workouts from './newcomp';

const CTS_FT_POWER = 280;
const START = new Date(2018, 0, 22);
const NOW = new Date();
const TODAY = new Date(NOW.getFullYear(), NOW.getMonth(), NOW.getDate());

const hhmmss = minutes => new Date(minutes * 60 * 1000).toISOString().substr(11, 8);
const watts = power => Math.ceil((power * CTS_FT_POWER) / 5) * 5;

const printWorkout = (workout, date) => {
  console.log(`${workout.name} ${date.toLocaleDateString()}\n${workout.type.name}`);
  let minutes = 0;
  if (workout.segments) {
    workout.segments.forEach((segment) => {
      const name = segment.type.shortName;
      const start = `${hhmmss(minutes)}`;
      const wattRange = `${watts(segment.type.power[0])}-${watts(segment.type.power[1])}w`;
      const rpmRange = `${segment.type.cadence[0]}-${segment.type.cadence[1]}rpm`;
      const duration = `${segment.duration} min`;
      console.log(`${name} ${start} - ${wattRange} @${rpmRange} for ${duration}`);
      minutes += segment.duration;
    });
    console.log(`     ${hhmmss(minutes)} - done`);
  }
};

const numDays = (TODAY - START) / (60 * 60 * 24 * 1000);
if (false && numDays >= 0 && numDays < workouts.length) {
  printWorkout(workouts[numDays], TODAY);
} else {
  const date = START;
  workouts.forEach((workout) => {
    printWorkout(workout, date);
    date.setDate(date.getDate() + 1);
    console.log();
  });
}
